package br.com.concrete.desafio.service.imp;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.concrete.desafio.entity.User;
import br.com.concrete.desafio.exception.InvalidSessionException;
import br.com.concrete.desafio.exception.TokenInvalidException;
import br.com.concrete.desafio.exception.TokenRequiredException;
import br.com.concrete.desafio.exception.UserExistsException;
import br.com.concrete.desafio.repository.UserRepository;
import br.com.concrete.desafio.service.TokenService;
import br.com.concrete.desafio.service.UserService;
import br.com.concrete.desafio.util.CryptUtils;

@Service
@Transactional
public class UserServiceImp implements UserService{

	@Autowired
	private TokenService tokenService;

	@Autowired
	private UserRepository userRepository;
	
	public User create(User user) throws UserExistsException {
		validateEmailAlreadyExists(user);
		
		user.setPassword(CryptUtils.cryptPassword(user.getPassword()));
		user.setToken(tokenService.generateToken(user));
		user.setCreated(new Date());
		return userRepository.save(user);
	}

	public User read(final String id, final String token) throws TokenRequiredException, TokenInvalidException, InvalidSessionException {
		tokenService.validateTokenNotNull(token);
		
		User userDb = userRepository.findOne(id);
		
		tokenService.validateIsAuthorized(userDb, token);
		tokenService.validateSession(userDb);
	
		return userDb;
	}
	

	private void validateEmailAlreadyExists(User user) throws UserExistsException {
		if (user.getEmail() == null || user.getEmail().equals("") || userRepository.findByEmail(user.getEmail()) != null) {
			throw new UserExistsException();
		}
	}

}
