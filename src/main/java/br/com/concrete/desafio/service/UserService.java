package br.com.concrete.desafio.service;

import br.com.concrete.desafio.entity.User;
import br.com.concrete.desafio.exception.InvalidSessionException;
import br.com.concrete.desafio.exception.TokenInvalidException;
import br.com.concrete.desafio.exception.TokenRequiredException;
import br.com.concrete.desafio.exception.UserExistsException;

public interface UserService {
	User create(User user) throws UserExistsException;
	User read(String id, String token) throws TokenRequiredException, TokenInvalidException, InvalidSessionException;
}
