package br.com.concrete.desafio.service.imp;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.concrete.desafio.entity.User;
import br.com.concrete.desafio.exception.UserAuthenticationException;
import br.com.concrete.desafio.exception.UserNotExistsException;
import br.com.concrete.desafio.repository.UserRepository;
import br.com.concrete.desafio.service.LoginService;
import br.com.concrete.desafio.service.TokenService;
import br.com.concrete.desafio.util.CryptUtils;

@Service
public class LoginServiceImp implements LoginService{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TokenService tokenService;

	public User login(final User user) throws UserAuthenticationException, UserNotExistsException {
		validateEmailNotNull(user);
		
		User userDb = this.userRepository.findByEmail(user.getEmail());
		
		validateUserIsRegistered(userDb);
		
		checkPassword(user, userDb);
		
		userDb.setToken(tokenService.generateToken(user));
		
		Date now = getNowDate();
		userDb.setLastLogin(now);
		userDb.setModified(now);
		
		return this.userRepository.save(userDb);
	}

	private void checkPassword(final User user, User userDb) throws UserAuthenticationException {
		if (!CryptUtils.checkPassword(userDb.getPassword(), user.getPassword())) {
			throw new UserAuthenticationException();
		}
	}
	
	private void validateEmailNotNull(final User user) throws UserAuthenticationException {
		if (user.getEmail() == null || user.getEmail().equals("")) {
			throw new UserAuthenticationException();
		}		
	}

	private void validateUserIsRegistered(final User user) throws UserNotExistsException {
		if (user == null) {
			throw new UserNotExistsException();
		}
	}
	private Date getNowDate() {
		long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);
		return now;
	}
}
