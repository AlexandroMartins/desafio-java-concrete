package br.com.concrete.desafio.service;

import br.com.concrete.desafio.entity.User;
import br.com.concrete.desafio.exception.UserAuthenticationException;
import br.com.concrete.desafio.exception.UserNotExistsException;

public interface LoginService {
	User login(User user) throws UserAuthenticationException, UserNotExistsException;

}
