package br.com.concrete.desafio.service;

import br.com.concrete.desafio.entity.User;
import br.com.concrete.desafio.exception.InvalidSessionException;
import br.com.concrete.desafio.exception.TokenInvalidException;
import br.com.concrete.desafio.exception.TokenRequiredException;

public interface TokenService {

	public String generateToken(User user);
	
	public void validateSession(final User userDb) throws InvalidSessionException;
	
	public void validateIsAuthorized(final User userDb, final String token) throws TokenInvalidException;
	
	public void validateTokenNotNull(final String token) throws TokenRequiredException;
}
