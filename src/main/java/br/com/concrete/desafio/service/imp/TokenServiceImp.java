package br.com.concrete.desafio.service.imp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.concrete.desafio.entity.User;
import br.com.concrete.desafio.exception.InvalidSessionException;
import br.com.concrete.desafio.exception.TokenInvalidException;
import br.com.concrete.desafio.exception.TokenRequiredException;
import br.com.concrete.desafio.service.TokenService;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenServiceImp implements TokenService {
	
	@Value("${token.secret}")
	private String secret;
	
	@Value("${token.expiration.time}")
	private Long expirationTime;
	
	private SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
	
	public String generateToken(User user) {
		final JwtBuilder jwtBuilder = Jwts.builder()
				.setSubject(String.valueOf(user.getId()))
				.signWith(signatureAlgorithm, secret);
		return jwtBuilder.compact();
	}

	public void validateSession(final User userDb) throws InvalidSessionException{
		if (userDb.getLastLogin() != null && userDb.getLastLogin().getTime() < System.currentTimeMillis()-expirationTime) {
			throw new InvalidSessionException();
		}
	}	
	
	public void validateIsAuthorized(final User userDb, final String token) throws TokenInvalidException{
		if (userDb == null || userDb.getToken() == null || !userDb.getToken().equals(token)) {
			throw new TokenInvalidException();
		}
	}	
	
	public void validateTokenNotNull(final String token) throws TokenRequiredException{
		if (token == null || token.equals("")) {
			throw new TokenRequiredException();
		}
	}	
	
}
