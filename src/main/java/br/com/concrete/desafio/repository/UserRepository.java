package br.com.concrete.desafio.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.concrete.desafio.entity.User;

public interface UserRepository extends CrudRepository<User, String> {

	User findByEmail(String email);
}