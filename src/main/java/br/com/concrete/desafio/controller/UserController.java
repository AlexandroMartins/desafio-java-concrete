package br.com.concrete.desafio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.concrete.desafio.entity.User;
import br.com.concrete.desafio.exception.ErrorResponse;
import br.com.concrete.desafio.exception.InvalidSessionException;
import br.com.concrete.desafio.exception.TokenInvalidException;
import br.com.concrete.desafio.exception.TokenRequiredException;
import br.com.concrete.desafio.exception.UserAuthenticationException;
import br.com.concrete.desafio.exception.UserExistsException;
import br.com.concrete.desafio.exception.UserNotExistsException;
import br.com.concrete.desafio.service.LoginService;
import br.com.concrete.desafio.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "User Controller", description = "Operations about user")
public class UserController {
	

	@Autowired
	private UserService userService;
	
	@Autowired
	private LoginService loginService;
	
	@ApiOperation(value="create a user")
	@RequestMapping(value="/user", method=RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Object> create(@RequestBody(required=true) final User user) {
		try {
			return new ResponseEntity<Object>(userService.create(user), HttpStatus.OK);	
		}catch(UserExistsException e){
			return returnErrorResponse(e, HttpStatus.FORBIDDEN);
		}
    }
	
	@ApiOperation(value="read a user profile")
	@RequestMapping(value="/user/{id}", method=RequestMethod.GET)
    public  ResponseEntity<Object> read(@PathVariable("id") final String id, @RequestHeader(name="token") final String token){
    	
		try {
			return new ResponseEntity<Object>(userService.read(id, token), HttpStatus.OK);	
		}catch(TokenRequiredException e){
			return returnErrorResponse(e, HttpStatus.BAD_REQUEST);
		} catch (TokenInvalidException e) {
			return returnErrorResponse(e, HttpStatus.UNAUTHORIZED);
		}catch (InvalidSessionException e) {
			return returnErrorResponse(e, HttpStatus.UNAUTHORIZED);
		}
    }
	
	@ApiOperation(value="user login")
	@RequestMapping(value="/login", method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Object> login(@RequestBody(required=true) final User user)  {
		try {
			return new ResponseEntity<Object>(loginService.login(user), HttpStatus.OK);	
		}catch(UserAuthenticationException e){
			return returnErrorResponse(e, HttpStatus.BAD_REQUEST);
		} catch (UserNotExistsException e) {
			return returnErrorResponse(e, HttpStatus.NOT_FOUND);
		}
    }

	private ResponseEntity<Object> returnErrorResponse(Exception e, HttpStatus httpStatus) {
		ErrorResponse errorResponse = getErrorResponse(e.getMessage(), httpStatus);
		return new ResponseEntity<Object>(errorResponse, httpStatus);
	}

	private ErrorResponse getErrorResponse(String msg, HttpStatus httpStatus) {
		/* lombok builder is blocking the gradlebuild.
		return ErrorResponse.builder()
		.msg(msg)
		.httpStatusCode(httpStatus.value())
		.build();
		*/
		
		return new ErrorResponse(msg, httpStatus.value());
	}
}
