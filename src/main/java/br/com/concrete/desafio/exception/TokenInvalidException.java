package br.com.concrete.desafio.exception;

public class TokenInvalidException extends RuntimeException{
	
	private static final long serialVersionUID = -2181461750220916704L;

	public TokenInvalidException(){
        super("Não autorizado");
    }
}
