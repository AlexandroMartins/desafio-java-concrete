package br.com.concrete.desafio.exception;

public class UserAuthenticationException extends RuntimeException {

	private static final long serialVersionUID = -4825658059042919419L;

	public UserAuthenticationException(){
        super("Usuário e/ou senha inválidos");
    }
}