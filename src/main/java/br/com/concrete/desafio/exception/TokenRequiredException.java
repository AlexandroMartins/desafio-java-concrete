package br.com.concrete.desafio.exception;

public class TokenRequiredException extends RuntimeException{

	private static final long serialVersionUID = -9125968195604222314L;

	public TokenRequiredException(){
        super("Não autorizado");
    }
}
