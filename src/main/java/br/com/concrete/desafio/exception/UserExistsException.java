package br.com.concrete.desafio.exception;

public class UserExistsException extends RuntimeException {

	private static final long serialVersionUID = 7270249223238582656L;

	public UserExistsException(){
        super("E-mail já existente");
    }
}