package br.com.concrete.desafio.exception;

public class InvalidSessionException extends RuntimeException{
	
	private static final long serialVersionUID = 3453253961309531119L;

	public InvalidSessionException(){
        super("Sessão inválida");
    }
}
