package br.com.concrete.desafio.exception;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {
	
	@JsonProperty("mensagem")
	private String msg;
	
	@JsonProperty("status")
	private Integer httpStatusCode;

	public ErrorResponse(String msg, Integer httpStatusCode) {
		super();
		this.msg = msg;
		this.httpStatusCode = httpStatusCode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getHttpStatusCode() {
		return httpStatusCode;
	}

	public void setHttpStatusCode(Integer httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
}
