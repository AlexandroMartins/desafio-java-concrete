package br.com.concrete.desafio.exception;

import javassist.NotFoundException;

public class UserNotExistsException extends NotFoundException {

	private static final long serialVersionUID = 7490177649546657957L;

	public UserNotExistsException(){
        super("Usuário e/ou senha inválidos");
    }
}