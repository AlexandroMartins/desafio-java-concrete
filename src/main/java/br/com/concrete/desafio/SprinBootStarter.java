package br.com.concrete.desafio;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprinBootStarter {

	public static void main(String[] args) {
		SpringApplication.run(SprinBootStarter.class, args);
	}

}

